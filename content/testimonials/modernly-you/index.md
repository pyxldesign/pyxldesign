---
title: 'Ashley - Modernly You'
date: June, 26, 2021
description: ''
draft: false
thumbnail: 'images/thumbnail.jpg'
weight: 30
---

I had the pleasure of working with Chris from Pyxl Design on the launch of my first website, Modernly You. Chris is one of the most talented and patient individuals that you will ever work with. His work exceeded my expectations. There really is nothing that he can't do. If you are on the fence about whether or not to make the investment on a website of your own, my advice would be to DO IT! The quality of work that comes out of Pyxl Design is worth every penny!
