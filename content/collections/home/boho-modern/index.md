---
title: 'Boho Modern Collection - Pyxl Design'
date: June, 24, 2021
collection: 'Boho Modern'
description: "Boho Modern is a clean and modern template. It is perfect for interior designers, bloggers and DIYers who are looking for a modern way to showcase their services. This template can be customized to represent your business' vision and goals. Perfect for those at the beginning of their business journey or those who are looking to refresh their existing brand."
draft: false
thumbnail: 'images/boho-1.webp'
imagePrefix: 'boho'
weight: 10

# Params
price: 499
currency: 'USD'
includes: ['Fully Responsive + Mobile Design', 'Home Page', 'Services Page', 'About Page', 'Contact Page', 'Blog Archive Page', 'Blog Page', 'Instagram Link Tree Page', 'Social Links in Header', 'Custom Instagram Feed in Footer', 'Ability to Customize Color Scheme', 'Basic Search Engine Optimization']
addons: ['eCommerce', 'Additional Pages', 'Monthly Maintenance Plan', 'Let Us Know What You Need!']
---
