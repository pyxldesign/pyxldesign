---
title: 'Colonial Charm Collection - Pyxl Design'
date: June, 24, 2021
collection: 'Colonial Charm'
description: "Colonial Charm is a beautiful and welcoming template. It is perfect for interior designers, bloggers and DIYers who are ready to elevate their business in a fresh new way. This template can be customized to represent your business' vision and goals. Perfect for those at the beginning of their business journey or those who are looking to refresh their existing brand."
draft: false
thumbnail: 'images/colonial-1.webp'
imagePrefix: 'colonial'
weight: 30

price: 499
currency: 'USD'
includes: ['Fully Responsive + Mobile Design', 'Home Page', 'Services Page', 'About Page', 'Contact Page', 'Blog Archive Page', 'Blog Page', 'Instagram Link Tree Page', 'Customizable Home Page Buttons', 'Custom Instagram Feed in Footer', 'Ability to Customize Color Scheme', 'Basic Search Engine Optimization']
addons: ['eCommerce', 'Additional Pages', 'Monthly Maintenance Plan', 'Let Us Know What You Need!']
---
