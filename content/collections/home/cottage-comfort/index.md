---
title: 'Cottage Comfort Collection - Pyxl Design'
date: June, 24, 2021
collection: 'Cottage Comfort'
description: "With a unique layout, Cottage Comfort is a modern and engaging template design for interior designers, bloggers and DIYers who are ready to elevate their business in a fresh new way. This template can be customized to represent your business' vision and goals. Perfect for those at the beginning of their business journey or those who are looking to refresh their existing brand."
draft: false
thumbnail: 'images/cottage-1.webp'
imagePrefix: 'cottage'
weight: 20

price: 499
currency: 'USD'
includes: ['Fully Responsive + Mobile Design', 'Home Page', 'Services Page', 'About Page', 'Contact Page', 'Blog Archive Page', 'Blog Page', 'Instagram Link Tree Page', 'Social Links in Footer', 'Custom Instagram Feed in Footer', 'Ability to Customize Color Scheme', 'Basic Search Engine Optimization']
addons: ['eCommerce', 'Additional Pages', 'Monthly Maintenance Plan', 'Let Us Know What You Need!']
---
