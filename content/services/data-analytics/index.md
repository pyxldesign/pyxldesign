---
title: "Data Analytics"
date: June, 24, 2021
description: ""
draft: false
icon: "<svg class='service-tile--icon analytics' height='52.532' x='0px' y='0px' viewBox='0 0 32 32'><path class='cls-1' d='M30.8,3.2C30,2.4,28.9,2,27.8,2c-2.3,0-4.2,1.9-4.2,4.2v0c0,1.1,0.4,2.2,1.2,3L20.2,16 c-0.3-0.1-0.6-0.1-0.9-0.1c-0.9,0-1.7,0.3-2.4,0.8L14,14.4c0.4-0.6,0.5-1.3,0.5-2.1v0c0-2.3-1.9-4.2-4.2-4.2S6.1,10,6.1,12.4v0 c0,1.2,0.5,2.4,1.4,3.2l-2.8,6.1c-0.2,0-0.3,0-0.5,0c-2.3,0-4.2,1.9-4.2,4.2v0C0,28.1,1.9,30,4.2,30s4.2-1.9,4.2-4.2v0 c0-1.4-0.7-2.7-1.8-3.5l2.7-5.9c0.3,0.1,0.7,0.1,1,0.1c0.8,0,1.6-0.2,2.3-0.7l3,2.3c-0.3,0.6-0.5,1.3-0.5,1.9v0 c0,2.3,1.9,4.2,4.2,4.2s4.2-1.9,4.2-4.2v0c0-1.3-0.6-2.4-1.5-3.2l4.5-6.7c0.4,0.1,0.9,0.2,1.3,0.2c2.3,0,4.2-1.9,4.2-4.2v0 C32,5.1,31.6,4,30.8,3.2L30.8,3.2z M8.1,12.4c0-1.2,1-2.2,2.2-2.2s2.2,1,2.2,2.2v0c0,1.2-1,2.2-2.2,2.2S8.1,13.6,8.1,12.4L8.1,12.4 L8.1,12.4z M2,25.8c0-1.2,1-2.2,2.2-2.2s2.2,1,2.2,2.2v0.1c-0.1,1.2-1,2.1-2.2,2.1C3,28,2,27,2,25.8L2,25.8L2,25.8z M19.3,17.9 c1.2,0,2.2,1,2.2,2.2v0c0,1.2-1,2.2-2.2,2.2s-2.2-1-2.2-2.2v0C17.1,18.9,18.1,17.9,19.3,17.9z M30,6.2L30,6.2c0,1.2-1,2.2-2.2,2.2 s-2.2-1-2.2-2.2v0c0-1.2,1-2.2,2.2-2.2C29,4,30,5,30,6.2z'/></svg>"
weight: 80
---

Want to know how your site is performing and how fast your reach is expanding? We can enable a full suite of metrics for your pages so you can track and analyze visitor numbers, unique visits, time per page, number of pages per visit, common exit pages, campaign performance, keyword ranking, bounce rate, and more. Seeing results you aren’t happy with? We can tweak pages so you hit your goals.