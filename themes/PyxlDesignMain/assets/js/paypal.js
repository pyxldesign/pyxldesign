import * as params from "@params";
const price = params.price;
const collection = params.collection;

function initPaypalButton() {
   paypal.Buttons({
      style: {
         shape: 'rect',
         color: 'silver',
         layout: 'vertical',
         label: 'buynow',
         
      },

      createOrder: function(data, actions) {
         return actions.order.create({
            purchase_units: [{"description": `${collection} WordPress Theme`,"amount":{"currency_code":"USD","value": price }}]
         });
      },

      onApprove: function(data, actions) {
         return actions.order.capture().then(function(details) {
            const email = details.payer.email_address;
            const name = details.payer.name.given_name;
            const scheduleButton = `<a href="https://calendly.com/pyxldesign/kick-off?name=${name}&email=${email}" rel="noreferer" target="_blank" class="btn pyxl-btn"><span>Schedule Kick-Off<i class="fas fa-external-link-alt" style="margin-left: 8px;"></i></span></a>`;

            //== Hide Paypal Buttons
            $('#paypal-button-container').hide().attr('aria-hidden', 'true');
            
            // create a new XMLHTTPREQUEST
            var xhr = new XMLHttpRequest();
            // GET a callback when server responds
            xhr.addEventListener('load', () => {
               // Update the emailStatus
               if(xhr.responseText == "Success") {
                  $('#paypal-button-container').html(`Thank you ${name}! Your Purchase is Complete.<div class="btn-wrapper center">${scheduleButton}</div>`).show();
                  $('.lightbox--content').html(`<p>Hi ${name},</p><p>Your purchase has been completed! Please schedule a kick-off call with the link below.</p><div class="btn-wrapper center">${scheduleButton}</div>`);
                  $('#thank-you').fadeIn().attr('aria-hidden', 'false');
               } else {
                  $('#paypal-button-container').html(`Thank you ${name}! Your Purchase is Complete.<div class="btn-wrapper center">${scheduleButton}</div>`).show();
                  return;
               }
            });
            xhr.open('GET', `https://api.pyxldesign.com/sendemail/paypal.php?email=${email}&name=${name}&collection=${collection}`);
            // Send Request
            xhr.send();
         });
      },

      onError: function(err) {
         console.log(err);
      }
   }).render('#paypal-button-container')
}

initPaypalButton();
