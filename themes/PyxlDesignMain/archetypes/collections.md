---
title: "{{ replace .Name "-" " " | title }} Collection - Pyxl Design"
date: {{ now.Format "January, 2, 2006" }}
collection: "{{ replace .Name "-" " " | title }}"
description: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore"
draft: true
thumbnail: "images/example-1.jpg"
imagePrefix: "exmaple"
weight: 10

[params]
   price: 500.00
   currency: "USD"
   includes: [
      "Item one",
      "Item two",
      "Item three",
      "Item four",
      "Item five",
      "Item six",
   ]
   addons: [
      "Addon one",
      "Addon two",
      "Addon three",
      "Addon four",
      "Addon five",
      "Addon six",
      "Addon seven",
   ]

---
